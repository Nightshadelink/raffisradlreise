---
title: "Bulgarien"
date: 2022-03-27T01:00:00Z
draft: false
description: "Bulgarien ist eine Republik in Südosteuropa mit etwa 6,5 Millionen Einwohnern. Das Land nimmt den gesamten östlichen Teil der Balkanhalbinsel ein und grenzt im Norden an Rumänien, im Westen an Serbien und Nordmazedonien, im Süden an Griechenland und die Türkei und im Osten an das Schwarze Meer."

weight: 1
categories: ["Bulgarien"]

thumbnail: "images/post/bulgaria.jpg"

#blog_info:
#- title: "Types :"
  #content: "Design Tool, Prototype, Development Tools, App, Software"
#- title: "Colors :"
  #content: "Blue, Purple, White, Orange"

blog_images:
- image: "images/post/bulgaria.jpg"
---

Bulgarien ist eine Republik in Südosteuropa mit etwa 6,5 Millionen Einwohnern. Das Land nimmt den gesamten östlichen Teil der Balkanhalbinsel ein und grenzt im Norden an Rumänien, im Westen an Serbien und Nordmazedonien, im Süden an Griechenland und die Türkei und im Osten an das Schwarze Meer. Bulgarien umfasst ein Gebiet von 110.994 Quadratkilometern und liegt in der gemäßigten Klimazone. Sofia ist die Hauptstadt und gleichzeitig die größte Stadt des Landes; andere größere Städte sind Plowdiw, Warna und Burgas.

Auf dem Gebiet des heutigen Bulgariens befinden sich die bislang frühesten Belege für die Anwesenheit des Menschen (Homo sapiens) in Europa und mit der neolithischen Karanowo-Kultur, die bis ins Jahr 6.500 v. Chr. zurückreicht, eine der frühesten Siedlungen des Kontinents. Im 6. bis 3. Jahrhundert v. Chr. geriet die Region ins Spannungsfeld der Thraker, Perser, Kelten und Griechen. Stabilität kam, als es dem Römischen Reich im Jahr 45 n. Chr. gelang, die Region zu erobern. Mit dem Niedergang und der Aufteilung des Reiches begannen in der Region erneut Invasionen unterschiedlicher Gruppen. Im 4. Jahrhundert wanderten die Goten ein und erschufen hier die einzige Schriftquelle ihrer Sprache. Um das 6. Jahrhundert wurden die Gebiete von den frühen Slawen besiedelt. Die Ur-Bulgaren, angeführt von den Brüdern Asparuch und Kuwer, verließen das Gebiet des (alten) (Groß-)Bulgariens und siedelten sich im späten 7. Jahrhundert dauerhaft auf der Balkanhalbinsel an. Sie gründeten zwei Reiche mit dem Namen Bulgarien, eines zwischen Donau und Balkangebirge und eines im Gebiet um das heutige Bitola im Westen der Halbinsel. Das Donaureich, das 681 vom Oströmischen Reich vertraglich anerkannt wurde, vereinigte sich im Laufe der Zeit mit dem Reich von Kuwer. Dieses Erste Bulgarische Reich beherrschte den größten Teil der südlichen Balkanhalbinsel und beeinflusste die slawischen Kulturen maßgeblich durch die Entwicklung der kyrillischen Schrift am Hofe der bulgarischen Zaren und die Gründung des Bulgarischen Patriarchats. Das Reich existierte bis Anfang des 11. Jahrhunderts, als der byzantinische Kaiser Basilius II. es eroberte und unterwarf. Ein erfolgreicher bulgarischer Aufstand im Jahr 1185 begründete ein Zweites Bulgarisches Reich, das unter Iwan Asen II. (1218–1241) seinen Höhepunkt erreichte. Nach zahlreichen erschöpfenden Kriegen und Feudalkämpfen löste sich das Reich 1396 auf und die Region geriet fast fünf Jahrhunderte lang unter osmanische Herrschaft.

Das heutige Bulgarien entstand 1878 im Zuge des Russisch-Osmanischen Krieges (1877–1878) und des Zerfalls des Osmanischen Reiches zunächst als autonomes Fürstentum und nach der Ausrufung der Unabhängigkeit (1908) als Zarentum Bulgarien. Im Zweiten Weltkrieg wurde Bulgarien von den Sowjets besetzt, die Monarchie abgeschafft und eine kommunistische Volksrepublik ausgerufen, die mit dem Zerfall des Kommunismus 1991 aufgelöst wurde.

Heute ist Bulgarien eine parlamentarische Republik, die aus 28 Provinzen mit einem hohen Grad an politischer, administrativer und wirtschaftlicher Zentralisierung verfügt. Mit einer Wirtschaft im oberen mittleren Einkommensbereich nimmt Bulgarien im Human Development Index den 56. Platz ein. Seine Marktwirtschaft ist Teil des europäischen Binnenmarktes und basiert weitgehend auf Dienstleistungen, gefolgt von Industrie – insbesondere Maschinenbau und Bergbau – und Landwirtschaft. Bulgarien ist der weltweit größte Lavendelölproduzent und hat eine lange Tradition im Rosenanbau und der Rosenölherstellung. Das Land ist mit einer demografischen Krise konfrontiert, da seine Bevölkerung seit etwa 1990 jährlich schrumpft. Verglichen mit einem Höchststand von fast neun Millionen Einwohnern im Jahr 1988 zählt es heute nur etwa 6,5 Millionen. Bulgarien ist seit 29. März 2004 Mitglied der NATO und seit 1. Januar 2007 Mitglied der Europäischen Union (EU) und des Europarates, Gründungsmitglied der OSZE und hatte dreimal einen Sitz im Sicherheitsrat der Vereinten Nationen eingenommen.
