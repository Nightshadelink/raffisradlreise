---
title: "Kroatien"
date: 2022-03-27T01:00:00Z
draft: false
description: "Kroatien ist ein Staat in der Übergangszone zwischen Mittel- und Südosteuropa. Kroatien ist Mitglied der Europäischen Union, der NATO, der Welthandelsorganisation, der OSZE und der Vereinten Nationen. Hauptstadt und größte Stadt ist Zagreb, zu den weiteren Großstädten zählen Split, Rijeka und Osijek."

weight: 1
categories: ["Kroatien"]

thumbnail: "images/post/croatia.jpg"

#blog_info:
#- title: "Types :"
  #content: "Design Tool, Prototype, Development Tools, App, Software"
#- title: "Colors :"
  #content: "Blue, Purple, White, Orange"

blog_images:
- image: "images/post/croatia.jpg"
---

Kroatien ist ein Staat in der Übergangszone zwischen Mittel- und Südosteuropa. Kroatien ist Mitglied der Europäischen Union, der NATO, der Welthandelsorganisation, der OSZE und der Vereinten Nationen. Hauptstadt und größte Stadt ist Zagreb, zu den weiteren Großstädten zählen Split, Rijeka und Osijek.

Das Staatsgebiet liegt östlich des Adriatischen Meeres und zum Teil im Südwesten der Pannonischen Tiefebene. Es grenzt im Nordwesten an Slowenien, im Norden an Ungarn, im Nordosten an Serbien, im Osten an Bosnien und Herzegowina und im Südosten an Montenegro. Das Gebiet der einstigen Republik Ragusa (Dubrovačka Republika), das heute den südlichsten Teil des Staates ausmacht, verfügt über keine direkte Landverbindung zum übrigen Staatsgebiet, da der wenige Kilometer breite Meereszugang von Bosnien und Herzegowina dazwischen liegt; das Gebiet um Dubrovnik bildet damit die einzige Exklave des Landes.
