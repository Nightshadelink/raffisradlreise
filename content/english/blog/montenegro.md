---
title: "Montenegro"
date: 2022-03-27T01:00:00Z
draft: false
description: "Montenegro ist eine Republik an der südöstlichen Adriaküste in Südosteuropa. Das montenegrinische Staatsgebiet grenzt im äußersten Westen an Kroatien, im Nordwesten an Bosnien und Herzegowina und im Nordosten an Serbien, im Südosten an den Kosovo und im Süden an Albanien."

weight: 1
categories: ["Montenegro"]

thumbnail: "images/post/montenegro.jpg"

#blog_info:
#- title: "Types :"
  #content: "Design Tool, Prototype, Development Tools, App, Software"
#- title: "Colors :"
  #content: "Blue, Purple, White, Orange"

blog_images:
- image: "images/post/montenegro.jpg"
---

Montenegro ist eine Republik an der südöstlichen Adriaküste in Südosteuropa. Das montenegrinische Staatsgebiet grenzt im äußersten Westen an Kroatien (25 km), im Nordwesten an Bosnien und Herzegowina (225 km) und im Nordosten an Serbien (124,4 km), im Südosten an den Kosovo (78,6 km) und im Süden an Albanien (172 km). Am 3. Juni 2006 wurde Montenegro erneut unabhängig; zuvor hatte es seit 1920 zu Jugoslawien gehört.

Der Balkanstaat ist mit etwa 622.000 Einwohnern und einer Fläche von 13.812 Quadratkilometern einer der kleineren Staaten Europas – etwas kleiner als Schleswig-Holstein. Die Hauptstadt und größte Stadt ist Podgorica, zweitgrößte Stadt ist Nikšić. Hauptwirtschaftszweige sind der Dienstleistungssektor und der Tourismus, vor allem an der montenegrinischen Küste.

Montenegro ist Mitglied der Vereinten Nationen, der WTO, der OSZE, des Europarates und der NATO. Montenegro ist Beitrittskandidat der Europäischen Union; es verwendet den Euro als Währung.
