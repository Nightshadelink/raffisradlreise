---
title: "Bosnien und Herzegowina"
date: 2022-03-27T01:00:00Z
draft: false
description: "Bosnien und Herzegowina ist ein südosteuropäischer Bundesstaat. Er besteht geografisch aus der Region Bosnien im Norden, die rund 80 Prozent des Staatsgebiets einnimmt, und der kleineren Region Herzegowina im Süden."

weight: 1
categories: ["Bosnien und Herzegowina"]

thumbnail: "images/post/bosnia_and_herzegovina.jpg"

#blog_info:
#- title: "Types :"
  #content: "Design Tool, Prototype, Development Tools, App, Software"
#- title: "Colors :"
  #content: "Blue, Purple, White, Orange"

blog_images:
- image: "images/post/bosnia_and_herzegovina.jpg"
---

Bosnien und Herzegowina ist ein südosteuropäischer Bundesstaat. Er besteht geografisch aus der Region Bosnien im Norden, die rund 80 Prozent des Staatsgebiets einnimmt, und der kleineren Region Herzegowina im Süden. Politische Teilgebiete des Bundesstaates sind die Republika Srpska, die Föderation Bosnien und Herzegowina sowie der Distrikt Brčko als Sonderverwaltungsgebiet.

Das Staatsgebiet liegt östlich des Adriatischen Meeres auf der Balkanhalbinsel und befindet sich nahezu komplett im Dinarischen Gebirge. Nachbarstaaten sind im Norden und Westen Kroatien, im Osten Serbien, und Montenegro im Südosten. Die bosnisch-herzegowinische Bevölkerung betrug 2013 gut 3,5 Millionen (siehe Bosnier und Herzegowiner). Hauptstadt und größte Stadt des Landes ist Sarajevo, weitere Großstädte sind Banja Luka, Tuzla, Zenica, Bijeljina und Mostar.
