---
title: "Georgien"
date: 2022-03-27T01:00:00Z
draft: false
description: "Georgien ist ein eurasischer Staat im Südkaukasus, östlich des Schwarzen Meeres und südlich des Großen Kaukasus gelegen. Im Norden wird er von Russland, im Süden von der Türkei und Armenien, im Osten von Aserbaidschan begrenzt."

weight: 1
categories: ["Georgien"]

thumbnail: "images/post/georgia.jpg"

#blog_info:
#- title: "Types :"
  #content: "Design Tool, Prototype, Development Tools, App, Software"
#- title: "Colors :"
  #content: "Blue, Purple, White, Orange"

blog_images:
- image: "images/post/georgia.jpg"
---

Georgien ist ein eurasischer Staat im Südkaukasus, östlich des Schwarzen Meeres und südlich des Großen Kaukasus gelegen. Im Norden wird er von Russland, im Süden von der Türkei und Armenien, im Osten von Aserbaidschan begrenzt. Die Landesteile Abchasien und Südossetien sind abtrünnig und werden nur von Russland und einigen weiteren Staaten als souverän anerkannt.

Mit rund 3,7 Millionen Einwohnern (2019) auf einer Fläche von 57.215 km² (ohne die abtrünnigen Landesteile) ist Georgien eher dünn besiedelt. Mehr als ein Viertel der Bevölkerung lebt in der Hauptstadtregion um Tiflis, weitere große Städte sind Batumi, Kutaissi und Rustawi.
