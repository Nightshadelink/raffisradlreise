---
title: "Datenschutzerklärung"
description: "this is meta description"
draft: false
---

Verantwortlicher im Sinne der Datenschutzgesetze, insbesondere der EU-Datenschutzgrundverordnung (DSGVO), ist:

Raffael Jansen
Wallbergstraße 6
83679 Sachsenkam
rafaeljansencoc@gmail.com
+49 176 56946277

## Angaben gemäß § 5 TMG:

Raffael Jansen
Postanschrift:

Wallbergstraße 6
83679 Sachsenkam
Kontakt:

Telefon: +49 176 56946277
E-Mail: rafaeljansencoc@gmail.com


Hinweise zur Website

Urheberrechtliche Hinweise

Raffael Jansen
Wallbergstraße 6
83679 Sachsenkam
rafaeljansencoc@gmail.com
+49 176 56946277

Verantwortlich für journalistisch-redaktionelle Inhalte ist:

Raffael Jansen
Wallbergstraße 6
83679 Sachsenkam
rafaeljansencoc@gmail.com
+49 176 56946277

### Ihre Betroffenenrechte

Unter den angegebenen Kontaktdaten unseres Datenschutzbeauftragten können Sie jederzeit folgende Rechte ausüben:

* Auskunft über Ihre bei uns gespeicherten Daten und deren Verarbeitung (Art. 15 DSGVO),
* Berichtigung unrichtiger personenbezogener Daten (Art. 16 DSGVO),
* Löschung Ihrer bei uns gespeicherten Daten (Art. 17 DSGVO),
* Einschränkung der Datenverarbeitung, sofern wir Ihre Daten aufgrund gesetzlicher Pflichten noch nicht löschen dürfen (Art. 18 DSGVO),
* Widerspruch gegen die Verarbeitung Ihrer Daten bei uns (Art. 21 DSGVO) und
* Datenübertragbarkeit, sofern Sie in die Datenverarbeitung eingewilligt haben oder einen Vertrag mit uns abgeschlossen haben (Art. 20 DSGVO).

Sofern Sie uns eine Einwilligung erteilt haben, können Sie diese jederzeit mit Wirkung für die Zukunft widerrufen.

Sie können sich jederzeit mit einer Beschwerde an eine Aufsichtsbehörde wenden, z. B. an die zuständige Aufsichtsbehörde des Bundeslands Ihres Wohnsitzes oder an die für uns als verantwortliche Stelle zuständige Behörde.

Eine Liste der Aufsichtsbehörden (für den nichtöffentlichen Bereich) mit Anschrift finden Sie unter: https://www.bfdi.bund.de/DE/Service/Anschriften/Laender/Laender-node.html.

### Verwendung von Scriptbibliotheken (Google Webfonts)

Um unsere Inhalte browserübergreifend korrekt und grafisch ansprechend darzustellen, verwenden wir auf dieser Website „Google Web Fonts“ der Google LLC (1600 Amphitheatre Parkway, Mountain View, CA 94043, USA; nachfolgend „Google“) zur Darstellung von Schriften.

Weitere Informationen zu Google Web Fonts finden Sie unter https://developers.google.com/fonts/faq und in der Datenschutzerklärung von Google: https://www.google.com/policies/privacy/.

### SSL-Verschlüsselung

Um die Sicherheit Ihrer Daten bei der Übertragung zu schützen, verwenden wir dem aktuellen Stand der Technik entsprechende Verschlüsselungsverfahren (z. B. SSL) über HTTPS.

---

### Information über Ihr Widerspruchsrecht nach Art. 21 DSGVO

#### Einzelfallbezogenes Widerspruchsrecht

Sie haben das Recht, aus Gründen, die sich aus Ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung Sie betreffender personenbezogener Daten, die aufgrund Art. 6 Abs. 1 lit. f DSGVO (Datenverarbeitung auf der Grundlage einer Interessenabwägung) erfolgt, Widerspruch einzulegen; dies gilt auch für ein auf diese Bestimmung gestütztes Profiling im Sinne von Art. 4 Nr. 4 DSGVO.

Legen Sie Widerspruch ein, werden wir Ihre personenbezogenen Daten nicht mehr verarbeiten, es sei denn, wir können zwingende schutzwürdige Gründe für die Verarbeitung nachweisen, die Ihre Interessen, Rechte und Freiheiten überwiegen, oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.

#### Empfänger eines Widerspruchs

Angaben gemäß § 5 TMG:


Raffael Jansen
Postanschrift:

Wallbergstraße 6
83679 Sachsenkam
Kontakt:

Telefon: +49 176 56946277
E-Mail: rafaeljansencoc@gmail.com


Hinweise zur Website

Urheberrechtliche Hinweise

Raffael Jansen
Wallbergstraße 6
83679 Sachsenkam
rafaeljansencoc@gmail.com
+49 176 56946277

Verantwortlich für journalistisch-redaktionelle Inhalte ist:

Raffael Jansen
Wallbergstraße 6
83679 Sachsenkam
rafaeljansencoc@gmail.com
+49 176 56946277

---

### Änderung unserer Datenschutzbestimmungen

Wir behalten uns vor, diese Datenschutzerklärung anzupassen, damit sie stets den aktuellen rechtlichen Anforderungen entspricht oder um Änderungen unserer Leistungen in der Datenschutzerklärung umzusetzen, z.B. bei der Einführung neuer Services. Für Ihren erneuten Besuch gilt dann die neue Datenschutzerklärung.

### Fragen an den Datenschutzbeauftragten

Wenn Sie Fragen zum Datenschutz haben, schreiben Sie uns bitte eine E-Mail oder wenden Sie sich direkt an die für den Datenschutz verantwortliche Person in unserer Organisation:

Die Datenschutzerklärung wurde mithilfe der activeMind AG erstellt, den Experten für externe Datenschutzbeauftragte (Version #2020-09-30).
