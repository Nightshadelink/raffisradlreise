---
title: "About"
layout: "about"
image: "images/about.jpg"
background_shape: "images/banner-shape.png"
draft: false

# My Experiences
my_experiences:
  enable: true
  title: "Die Route"
  description: "Die Route die wir nehmen werden"
  #TODO: Karten verrücken/versetzen

  experiences_item:
  - title: "Italien"
    gefahrene_kilometer: "~300"
    time_period: "5"
    link: "/blog/italy"

  - title: "Slowenien"
    gefahrene_kilometer: "~30"
    time_period: "1"
    link: "/blog/slovenia"

  - title: "Kroatien"
    gefahrene_kilometer: "~600"
    time_period: "12"
    link: "/blog/croatia"

  - title: "Bosnien und Herzegowina"
    gefahrene_kilometer: "0"
    time_period: "0"
    link: "/blog/bosnia_and_herzegovina"

  - title: "Montenegro"
    gefahrene_kilometer: "~100"
    time_period: "2"
    link: "/blog/montenegro"

  - title: "Albanien"
    gefahrene_kilometer: "~250"
    time_period: "3"
    link: "/blog/albania"

  - title: "Nordmazedonien"
    gefahrene_kilometer: "~210"
    time_period: "3"
    link: "/blog/north_macedonia"

  - title: "Griechenland"
    gefahrene_kilometer: "50"
    time_period: "10"
    link: "/blog/greece"

  - title: "Bulgarien"
    gefahrene_kilometer: "0"
    time_period: "0"
    link: "/blog/bulgaria"

  - title: "Türkei"
    gefahrene_kilometer: ""
    time_period: ""
    link: "/blog/turkey"

  - title: "Georgien"
    gefahrene_kilometer: ""
    time_period: ""
    link: "/blog/georgia"

  - title: "Aserbaidschan"
    gefahrene_kilometer: ""
    time_period: ""
    link: "/blog/azerbaijan"

  - title: "Iran"
    gefahrene_kilometer: ""
    time_period: ""
    link: "/blog/iran"

# About info
about_info:
  enable: false
  item:
  - name: "My Approach"
    content: "Lorem ipsum dolor sit amet, consectetur adipisdf bicing elit. Quas offiscs cuque, harum dicta neces sitatrrthr thrth iujhs reprehenderit, delsectsus molesdtiae, impedit alias adipi thsci distinctio volusd ptas. Tempora modi amet volufy jnfyp tatlje  provide nsdv sdvt solusfta consequatur. oresaam ipsum dolor sit amhet, consec dassetur  facere tempore soluta Lorsgem ipsum shghu ugisdvg srgvsrgv vswrgv srgt lias adipi thsci distiio voslusd"

  - name: "My Skills"
    content: "Lorem ipsum dolor sit amet, consectetur adipisdf bicing elit. Quas offiscs cuque, harum dicta neces sitatrrthr thrth iujhs reprehenderit, delsectsus molesdtiae, impedit alias adipi thsci distinctio volusd ptas. Tempora modi amet volufy jnfyp tatlje  provide nsdv sdvt solusfta consequatur. oresaam ipsum dolor sit amhet, consec dassetur  facere tempore soluta Lorsgem ipsum shghu ugisdvg srgvsrgv vswrgv srgt lias adipi thsci distiio voslusd"


# Compatibility
compatibility:
  enable: false
  title: "I Am Compatible With This Tools"
  item:
    - image: "images/compatibility/01.png"
    - image: "images/compatibility/02.png"
    - image: "images/compatibility/03.png"
    - image: "images/compatibility/04.png"
    - image: "images/compatibility/05.png"
    - image: "images/compatibility/06.png"
    - image: "images/compatibility/07.png"
    - image: "images/compatibility/08.png"
    - image: "images/compatibility/09.png"
    - image: "images/compatibility/10.png"
    - image: "images/compatibility/11.png"
    - image: "images/compatibility/12.png"

---

## Auf der Reise sind<br> <strong>Sam</strong> und <strong>Raffi</strong>

Wir nehmen euch hier mit auf unsere gemeinsame Reise.
Auf dieser Reise versuchen wir alles so gut wie möglich aufzunehmen und hier zu dokumentieren.

Kennengelernt haben wir uns im Studium in Rosenheim und sind seither unzertrennbare Kumpels!
Da wir bis zum Master ein halbes Jahr Zeit haben, haben wir uns für dieses Abenteuer entschieden.
