---
title: "Tag 38 | 05.05. | insg. 51 km"
image: "images/post/tag38/tag38.jpeg"
images: ["images/post/tag38/tag38_0.jpeg",
         "images/post/tag38/tag38_1.jpeg",
         "images/post/tag38/tag38_2.jpeg",
         "images/post/tag38/tag38_3.jpeg",
         "images/post/tag38/tag38_4.jpeg",
         "images/post/tag38/tag38_5.jpeg",
         "images/post/tag38/tag38_6.jpeg",
         "images/post/tag38/tag38_7.jpeg",
         "images/post/tag38/tag38_8.jpeg",
         "images/post/tag38/tag38_9.jpeg",
         "images/post/tag38/tag38_10.jpeg",
         "images/post/tag38/tag38_11.jpeg"
        ]
author: "Raffi"
date: 2022-05-05T23:00:00Z
categories: ["Türkei"]
tags: ["Reise", "Fahrrad"]
featured: true
---

In der Früh verabschieden wir uns von Alex und Sonja, die wir gestern kennen gelernt haben. Beim Einkaufen für’s Frühstück fällt uns auf, das die Türkei wesentlich günstiger als Griechenland ist. Das heißt wir können uns den Bauch vollschlagen. 

Die Strecke führte uns heute über holprige Feldwege und trotz Gegenwind war es heiß. Jedesmal wenn wir ein Dorf durchqueren werden wir freundlich gegrüßt. Schließlich machen wir Fast in einem Cafe und werden zu Chai und kaltem Wasser eingeladen. Die Kommunikation ist jedoch etwas schwierig, da niemand Englisch spricht und wir kein Türkisch. Zum Abschied bekommen wir sogar noch Gemüse geschenkt. Auf dem Weg zu den nächsten Dörfern sehen wir viele Hirten mit ihren Schafen und erstaunlich große Schlangen auf den Straßen. Abends fragen wir in einem kleinen Dorf ob wir in der Moschee übernachten dürfen. Das geht zwar nicht, aber im alten Parteigebäude daneben ist Platz für uns drei. Unser treuer spanischer Begleiter Mateo ist immernoch mit von der Partie. Der Bürgermeister lädt uns vor dem Abendessen noch zu ein Dutzend Chais ein und als wir Brot in seinem Laden kaufen wollen schenkt er es uns. Mit einem lachenden Herz von all der Gastfreundlichkeit gehen wir ins Bett 
