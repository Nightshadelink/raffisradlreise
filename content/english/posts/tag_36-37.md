---
title: "36. und 37. Tag | 3. – 4.5. | insg. 186 km"
image: "images/post/tag36-und-37/tag36.jpeg"
images: ["images/post/tag36-und-37/tag36-und-37_0.jpeg",
         "images/post/tag36-und-37/tag36-und-37_1.jpeg",
         "images/post/tag36-und-37/tag36-und-37_2.jpeg",
         "images/post/tag36-und-37/tag36-und-37_3.jpeg",
         "images/post/tag36-und-37/tag36-und-37_4.jpeg",
         "images/post/tag36-und-37/tag36-und-37_5.jpeg",
         "images/post/tag36-und-37/tag36-und-37_6.jpeg"
         ]
author: "Raffi"
date: 2022-05-03T23:00:00Z
categories: ["Griechenland"]
tags: ["Reise", "Fahrrad", "Alexandroupolis"]
featured: true
---

Letzte Nacht haben wir in einer verlassenen Reha-Klinik übernachtet. Diese wurde um die natürlichen heißen Quellen von Potamia herum errichtet. Anscheinend war dieses Geschäft wohl aber nicht rentabel und so stehen diese Gebäude mutmaßlich seit Jahrzehnten leer. Die Szenerie erinnert an ein Post-Apokalypse-Computerspiel wie Metro oder Last of us. In so mancher Ecke könnte man sich auch gruseln, aber nachdem wir hier zu dritt campieren mache ich mir nicht zu viele Gedanken.

Wir starten wie üblich in den Tag. Leider haben wir heute wieder mit einigem Gegenwind zu kämpfen und so ziehen die ersten 20 km nur sehr schleppend an uns vorbei. Da kommt der erste Lidl sehr gelegen. Mateo unser spanischer Mitreißender hält darüber hinaus stets Ausschau nach diesem deutschen Discounter, da die Artikel hier besonders günstig sind. Wir möchten heute auch das erste mal “Dumpsterdiving” ausprobieren. Das hat uns Maxim aus Frankreich am Vortag beigebracht. Und das funktioniert bei diesem Lidl tatsächlich ziemlich gut. Ich finde mehrere frische Orangen, Karotte, eine Avocado und sogar eine Mango im Müll-Container. Das wird eine üppige Mittagspause. 

Nach der Pause brechen wir weiter Richtung Alexandroupolis. Diesmal aber mit Rückenwind. Zwischendurch machen wir noch eine Pause an einem verlassenen Bahnhof, bevor wir 10 km vor Alexandroupolis in einem Olivenbaumfeld unseren Camp-Spot finden.

Der nächste Tag ist auch schon unser letzter Tag in Griechenland. Wir starten nach Alexandroupolis und machen ein paar Fotos vom kleinen Leuchtturm am Meer und danach geht es geradewegs auf die Grenze zur Türkei zu. Im letzten Dorf vor der Grenze treffen wir noch zwei weitere Radreisende: Alex und Sonja. Die beiden kommen aus Köln und haben Griechenland vom Süden kommend durchquert. Daher ist das auch ihr erster Grenzübertritt. Und der hats in sich. Insgesamt müssen wir dreimal unsere Reisepässe vorzeigen. Die schwer bewaffneten Soldaten sind aber alle Rechte freundlich und so können wir fünf ohne Probleme in die Türkei einreisen.

Alex und Sonja möchten anders als wir bei Çanakalle auf den asiatischen Kontinent übersetzen, deshalb werden sie morgen Richtung Süden weiterradeln. Heute zelten wir aber gemeinsam in einem Waldstück am Rande von Kesan.
