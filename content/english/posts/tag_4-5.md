---
title: "Tag 4 und 5, 49 km und 55 km"
image: "images/post/tag4.jpeg"
images: ["images/post/tag4-und-5_1.jpeg",
         "images/post/tag4-und-5_2.jpeg",
         "images/post/tag4-und-5_3.jpeg",
         "images/post/tag4-und-5_4.jpeg",
         "images/post/tag4-und-5_5.jpeg",
         "images/post/tag4-und-5_0.jpeg",
         ]
author: "Raffi"
date: 2022-04-02T01:00:00Z
categories: ["Italien"]
tags: ["Reise", "Fahrrad"]
featured: true
---

Auf unserem Weg nach Triest haben wir einen Zwischenstopp am “Castello di Miramare” eingelegt. Eine wunderschöne Schloßanlage direkt am Meer. Im Inneren hin sogar ein Gemälde von König Ludwig. Das Schloss und sein bezaubernder Garten gehört zu den Highlights der bisherigen Reise.
Nach einem kurzen Besuch der Altstadt von Triest ging zum Decathlon. Dort kauften wir noch fehlendes Equipment und einen neuen Lenker, für bessere Langstreckenperformance. Wir radelten am Meer entlang bis wir auf den “Parenzana” trafen. Er ist eine stillgelegte Bahnlinie, die von Italien über Slowenien nach Kroatien führt. Am Wegesrand fanden wir abends einen Schrebergarten, in dem wir unsere Zelte aufschlugen. Auch heute haben uns die Regenwolken begleitet.
Am fünften Tag ging es weiter auf dem Parenzana. Mit seinen vielen Tunneln und Brücken ist er ein eindrucksvoller Radweg. Auch heute hat es wieder geregnet, was unsere Stimmung aber nicht drücken konnte. Ich habe mittlerweile wasserdichte Überschuhe und muss nicht mehr mit Flipflops und kalten Wind fahren. Heute haben wir Slowenien hinter uns gelassen und die Grenze nach Kroatien überquert.
