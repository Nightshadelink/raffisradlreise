---
title: "Tag 46 und 47 | 13.-14.05. | insg. 103 km"
image: "images/post/tag46-und-47/tag46.jpeg"
images: ["images/post/tag46-und-47/tag46-und-47_0.jpeg",
         "images/post/tag46-und-47/tag46-und-47_1.jpeg",
         "images/post/tag46-und-47/tag46-und-47_2.jpeg",
         "images/post/tag46-und-47/tag46-und-47_3.jpeg",
         "images/post/tag46-und-47/tag46-und-47_4.jpeg",
         "images/post/tag46-und-47/tag46-und-47_5.jpeg",
         "images/post/tag46-und-47/tag46-und-47_6.jpeg",
         "images/post/tag46-und-47/tag46-und-47_7.jpeg",
         "images/post/tag46-und-47/tag46-und-47_8.jpeg"
        ]
author: "Raffi"
date: 2022-05-14T23:00:00Z
categories: ["Türkei"]
tags: ["Reise", "Fahrrad", "Istanbul", "Bandirma", "Warmshowers", "Selcuk"]
featured: true
---

Heute kürzen wir ab. Wir nehmen die Fähre von Istanbul nach Bandirma, weil man über die Bosporusbrücke nicht mit dem Fahrrad fahren darf. Von hier geht es dann mit dem Bus nach Izmir weiter. Wir fahren an der Strandpromenade entlang und schauen uns den Glockenturm an. Der letzte deutsche Kaiser hat hierfür die Glocken spendiert. Zum Sonnenuntergang empfängt uns Hüseyin, unser Warmshowerhost in seiner Wohnung. Wir trinken Bier miteinander und bekommen eine warme Dusche bereitgestellt. Am nächsten Morgen begleitet uns Schüsseln mit seinem Fahrrad. Leider ist die einzige gute Straße aus Izmir die Autobahn. Hier sind zwar eigentlich Fahrräder verboten, aber hier nimmt man das nicht so genau. Bei einem freundlichen Motorradfahrer kann ich mich einhaken und er zieht mich den Berg hinauf. Zur Mittagspause erreichen wir wieder das Meer, gehen baden und machen eine Siesta. In einem Café in Selcuk lernen wir Onur kennen. Er ist eine örtliche Bikerlegende, organisiert Turniere und gibt uns ganz viele Routentipps. 10 Kilometer südlich von Selcuk machen wir unser erstes Lagerfeuer und grillen frischen Fisch auf einem heißen Stein.