---
title: "Tag 32 und 33 | 29. – 30.04. | insg. 143 km"
image: "images/post/tag32-und-33/tag32.jpeg"
images: ["images/post/tag32-und-33/tag32-und-33_0.jpeg",
         "images/post/tag32-und-33/tag32-und-33_1.jpeg",
         "images/post/tag32-und-33/tag32-und-33_2.jpeg",
         "images/post/tag32-und-33/tag32-und-33_3.jpeg",
         "images/post/tag32-und-33/tag32-und-33_4.jpeg",
         ]
author: "Sam"
date: 2022-04-29T23:00:00Z
categories: ["Griechenland"]
tags: ["Reise", "Fahrrad", "Chalkidiki"]
featured: true
---

Heute wachen wir in einer heruntergekommenen Strandbar bei Nea Moudalia auf. Wir reisen weiter entlang der Küste der Chalkidiki. Diese Region in Nordgriechenland besitzt drei Landzungen, die wie „Finger“ ins Meer ragen. Wir haben vor, auf den zweiten der drei Finger, Sithonia, abzubiegen, um hier die Küste entlang zu radeln. Dieser ist bekannt für seine wunderschönen Sandstrände.

Das Wetter lässt heute aber nicht so sehr Strandurlaub-Stimmung aufkommen. Es ist den ganzen Tag über bewölkt und die Sonne lässt sich kaum blicken. Als wir dann vor der Entscheidung stehen, auf Sithonia abzubiegen, entscheiden wir uns wetterbedingt doch um und sparen uns du diese zwei Tage ein.

Der Tag verläuft sonst unspektakulär. Als wir nach 60 km eine Pause in einem Strand-Café einlegen, fragen wie gleich die Bedienung, ob wir hier unser Zelt aufschlagen können. Diese hat nichts dagegen und so verbringen wir noch einen schönen Abend mit Baden im Meer und anschließender Dusche.

Am nächsten Morgen werden wir unsanft geweckt. Der Besitzer des Strand-Cafés ist schon sehr früh vor Ort und möchte ein paar Instandhaltungsarbeiten durchführen. Deshalb müssen wir schnell unsere Sachen packen und das Feld räumen. Die Route startet gleich mit einem Berg und nach den ersten 3 km höre ich von hinten ein lautes „Oh ne!“. Raffi hat einen Platten. Aber nichts leichter als das und 30 min später sind wir wieder auf der Straße. Auch dieser Tag verläuft sonst recht ereignislos. Wir kommen an vielen schönen Sandstränden vorbei und stecken bei unserer Mittagspause die Füße in den Sand, bevor wir dann in Stavros den Radl-Tag beenden. Diesmal wird in einem halbfertigen Rohbau übernachtet mit Ausblick aufs Meer. Hierfür haben wir natürlich zuvor um Erlaubnis gefragt ;)
