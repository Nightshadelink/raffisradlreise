---
title: "Tag 58 | 25.05. | insg. 0 km"
image: "images/post/tag58/tag58.jpeg"
images: ["images/post/tag58/tag58_0.jpeg",
         "images/post/tag58/tag58_1.jpeg",
         "images/post/tag58/tag58_2.jpeg"
        ]
author: "Raffi"
date: 2022-05-25T23:00:00Z
categories: ["Türkei"]
tags: ["Reise", "Fahrrad", "Göreme"]
featured: true
---

Morgens kommen wir nach einer unruhigen Busfahrt in Göreme an. Die Region ist für ihre außergewöhnlichen Felsformation und die Ballonfahrten bekannt. Wir haben noch nicht einmal alle Taschen wieder ans Fahrrad gebaut, da treffen wir schon den ersten Bikepacker. Jonathan ist in Georgien gestartet und fährt zurück nach Stuttgart. Gemeinsam suchen wir ein Hostel und beschließen einen Powernap zu machen. In dem Höhlenhostel ist es schön kühl und leise. So passiert es, dass wir bis 17:00 Uhr schlafen. Nach diesem ausgeweiteten Mittagsschlaf treffen wir unseren alten Kumpel Matteo wieder und gehen gemeinsam Pide essen. Seit dem wir uns getrennt haben ist er den gesamten Weg hierher geradelt. Danach genießen wir den Sonnenuntergang auf einer der vielen Aussichtsplattformen und gehen früh schlafen.