---
title: "Tag 29 | 26.04 | ca. 93 km"
image: "images/post/tag29/tag29.jpeg"
images: ["images/post/tag29/tag29_0.jpeg",
         "images/post/tag29/tag29_1.jpeg",
         "images/post/tag29/tag29_2.jpeg",
         "images/post/tag29/tag29_3.jpeg",
         "images/post/tag29/tag29_4.jpeg",
         "images/post/tag29/tag29_5.jpeg",
         "images/post/tag29/tag29_6.jpeg",
         "images/post/tag29/tag29_7.jpeg"
         ]
author: "Sam"
date: 2022-04-26T23:00:00Z
categories: ["Nord Mazedonien"]
tags: ["Reise", "Fahrrad",]
featured: true
---

Morgens führte uns unsere Route am Ohrid Lake entlang, bis wir Samuel Festung erreichten. Eine der größten noch intakten Festungsanlagen in Nordmazedonien. Im nahegelegenen Souvenirshop lernen eine sympathische Frau in unserem Alter kennen und wir informieren uns über das Land. Sie erzählt uns, dass wir uns inmitten der Osterfeiertage befinden, weil in der orthodoxen Kirche an einem anderen Datum gefeiert wird. Nachdem wir auf der Landstraße Vollgas gegeben haben und bei heißem Wetter und steilen Anstiegen langsam kein Wasser mehr haben erreichen wir ein kleines Dorf. Hier können wir in der Moschee zu Mittag essen und finden sogar einen geöffneten Supermarkt.

Frisch gestärkt radeln wir nach Bitola und finden einen Schädel auf der Straße. Ist der von einem Pferd oder von einer Kuh? Kurz hinter der Stadt treffen wir uns abends mit Mateo. Dem Spanier den ich vor längerem in Skhoda kennengelernt habe. Wir schlagen unser Zelt auf dem Grund einer orthodoxen Gemeinde auf. Dort wohnt ein Pfarrer mit seinem sehr süßen Hund. Nach dem Abendessen beschützt uns unser neuer Bodyguard, während wir träumen.
