---
title: "Vor der Reise"
image: "images/post/vor_der_reise.jpeg"
author: "Sam"
date: 2022-03-28T01:00:00Z
categories: ["Italien"]
tags: ["Aufbruchstimmung", "Reise", "Auto", "Fahrrad"]
featured: true
---

Es war ziemlich genau vor einem Jahr, da haben wir das erste mal darüber gesprochen: Eine mehrmonatige Tour mit dem Fahrrad nach Abschluss unseres Bachelors. Inspiriert durch Filme wie „Biking Borders“ und „Zwei nach Shanghai“ der Gebrüder Höpner träumten auch wir von den unzähligen Abenteuern und Begegnungen einer großen Radtour.
Aber wer sind denn eigentlich „Wir“? „Wir“ besteht aus Raffi und Sam. Wir sind uns damals zu Beginn unseres Mechatronik-Studiums zum ersten Mal begegnet. Im Laufe unseres Studiums hat sich daraufhin eine innige Freundschaft entwickelt. Wir sind nicht immer einer Meinung und haben zu einigen Themen eine unterschiedliche Herangehensweise. Und doch einen uns zwei Eigenschaften, die in meinen Augen essenziell sind für eine Unternehmung von diesem Ausmaß: Wir sind beide sehr besonnen, was uns ermöglicht auch in schwierigen Situationen einen kühlen Kopf zu bewahren und wir sind beide sehr reflektiert. Diese Reflektion ermöglicht es uns, unsere Erwartungen an diese Reise abzugleichen und so finden wir einen Kompromiss, der uns beide erfüllt.
Im diesem Sinne gestaltete sich schon die Reisevorbereitung. Der ursprüngliche Plan, mit dem Fahrrad nach China zu radeln wurde aufgrund der Kürze der verfügbaren Zeit verworfen. Hinzu kamen politische Unruhen und sogar Kriege, die uns einige Routenpläne über den Haufen werfen ließen. Und dann waren da noch zwei Bachelorarbeiten, die fertig geschrieben werden mussten. So starteten wir Anfang März in unsere Reisevorbereitung. Es waren Fahrräder zu besorgen, Sachen einzukaufen, Impfschutz aufzubauen und das Allerwichtigste: Raffi musste noch seine Bachelorarbeit abgeben. Es spitzte sich alles zu auf den 28.03. An diesem Tag hatten wir beide unseren letzten Impftermin, Raffi hatte am Donnerstag zuvor sein Kolloquium und an diesem Tag bekam er auch sein Fahrrad zurück vom Service. Also wurde für diesen Tag kurzum ein Bus gebucht. Ein Bus? Ja, ein Bus, denn wir erhofften uns dadurch, die kalten Nächten in Deutschland und Österreich zu überspringen. Daher fahren wir erst mit dem Bus nach Venedig und dort startet dann unsere Reise. Erstmal durch die Balkanstaaten, dann durch die Türkei und danach schauen wir mal, wieviel Zeit uns noch bleibt.
Wir treffen uns also am 28.03. im Olympiadorf, um mit Patrick und Mary ein letztes mal in die Sauna zu gehen, bevor um 22:00 Uhr unser Flixbus abfährt. Doch was ist das?! An Raffis Fahrrad fehlt dich noch ein entscheidendes Teil….
