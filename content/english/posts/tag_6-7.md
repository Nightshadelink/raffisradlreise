---
title: "Tag 6 und 7, 75 km und 77 km"
image: "images/post/tag_6-7_5.jpeg"
images: ["images/post/tag_6-7_1.jpeg",
         "images/post/tag_6-7_2.jpeg",
         "images/post/tag_6-7_3.jpeg",
         "images/post/tag_6-7_4.jpeg",
         "images/post/tag_6-7_5.jpeg",
         ]
author: "Sam"
date: 2022-04-04T01:00:00Z
categories: ["Kroatien"]
tags: ["Reise", "Fahrrad"]
featured: true
---

Wir sind weiter auf dem Parenzana unterwegs. Am Vorabend haben wir hinter einer kleinen Baumgruppe direkt am Wegesrand unsere Zelte aufgeschlagen. Die Nacht war mit 1°C sehr kalt, dafür aber trocken. Hierfür haben wir uns jedoch extra dick angezogen in unseren Schlafsäcken und so war es trotzdem schön warm.
Um 9:00 Uhr treten wir gemeinsam mit Caro und Alex in die Pedale. Caro hat jedoch nach wenigen Kilometern ihren zweiten Platten Reifen innerhalb von zwei Tagen und so radeln Raffi und ich weiter, während die beiden den Reifen flicken. Wir sind verabredet, weiter den Tag über in Kontakt zu bleiben.
Da es sich beim Parenzana um eine ehemalige Bahnlinie handelt, fährt man hier auch durch die alten Bahntunnel und über die Viadukte der alten Bahnlinie. Diese stellen die Highlights unserer Etappe dar. Nachmittags gewinnt die Sonne an Kraft hinzu und wir können sogar unsere letzten nassen Klamotten unter der Fahrt trocknen. Der Parenzana endet in Porec. Hier fühlt sich unsere Reise deutlich mehr nach Urlaub an, wenn man das Meer unter der Sonne glitzern sieht. Wir fahren noch ein wenig an der Küste entlang und halten Ausschau nach einem Übernachtungsort. Die Küste ist jedoch dominiert von Hotelanlagen, die nur ungern Zelt-Gäste aufnehmen und so fahren wir nochmal 1 km ins Landesinnere, um in einem kleinen Waldstück unterzukommen. Dort sitzen wir gerade vor unserem Gaskocher und bereiten Nudeln mit Pesto zu, als plötzlich in 10 m Entfernung eine Rehherde vorbeiläuft. Wir sind uns einig: „Das war krass“.
Es war wieder eine sehr kalte Nacht. Diesmal habe ich aber nicht so gut geschlafen und starte auch nicht so gut in den Tag. In der Früh liegen die Temperaturen im einstelligen Bereich und wir müssen erstmal warm werden. Diesmal sind auch zum ersten mal Caro und Alex nicht dabei. Diese haben gestern Abend einen anderen Ort für ihr Zelt gefunden.
Zu Beginn fahren wir weiter an der Küste entlang, bevor wir nach Osten abbiegen, um den Lim Fjord zu umgehen. Hier folgen wir einer Staatsstraße, sodass vom Vormittag, abgesehen von einer 3-minütigen Abfahrt am Fjord entlang nichts Spektakuläres zu berichten ist. Nach dem darauffolgenden Aufstieg entscheiden wir uns nicht, an der anderen Seite des Kanals entlang, an die Küste zurückzukehren. Bei diesen kalten Temperaturen ist das Meer ohnehin nicht allzu attraktiv. Stattdessen sparen wir Pula aus und fahren noch weitere 50 km in Richtung Labin. Am Abend bauen wir unser Zelt dann beim kleinen Dorf Nedescina auf. So geht ein anstrengender Tag mit mehreren kräftezehrenden Anstiegen zu Ende. Wir sind stolz, dass wir trotzdem 77 km geschafft haben.
