---
title: "Tag 1, 60 km"
image: "images/post/tag_1_3.jpeg"
images: ["images/post/tag_1_3.jpeg",
         "images/post/tag_1_1.jpeg",
         "images/post/tag_1_2.jpeg",
         "images/post/tag_1_4.jpeg",
         ]
author: "Raffi"
date: 2022-03-29T01:00:00Z
categories: ["Italien"]
tags: ["Reise", "Fahrrad"]
featured: true
---

Für meinen vorderen Gepäckträger würden keine Halterungen mitgeliefert. Dank Sam’s Ingenieurskünsten haben wir ihn für den Start mit Kabelbindern fixiert.
Nachdem wir bei Patrick und Mary abends gegrillt haben und in der hauseigenen Sauna waren, ging es zum ZOB. Im stinkigen Flixbus nach Verona mit einem zweistündigen Aufenthalt um 4 Uhr morgens. Danach fuhr der Bus weiter nach Mestre. Nach einem erfolglosen Versuch, mit dem Fahrrad nach Venedig zu kommen, geben wir auf und machen uns stattdessen auf den Weg nach Jesolo. Während dir durch die wunderschön verlassene Landschaft radeln, treffen wir Caro und Alexis. Diese stammen aus Frankreich und haben genauso wie wir den Eurovelo 8 als Route gewählt. Mit dem neuen Freunden im Gepäck geht es weiter entlang der Küste. Auf dem Weg treffen wir sehr viele freundliche Radfahrer. Am frühen Abend finden wir einen Schlafplatz gegenüber eines verlassenen Nachtclubs hinter Jesolo. Ein erfolgreicher erster Tag geht zu Ende und wir liegen gemütlich in unseren Schlafsäcken. Doch was ist das?! Kurz vorm Einschlafen hören wir ein Geräusch…
