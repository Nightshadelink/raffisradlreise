---
title: "Tag 2 und 3, 77 km und 75 km"
image: "images/post/tag_3-4_3.jpeg"
images: ["images/post/tag_3-4_1.jpeg",
         "images/post/tag_3-4_2.jpeg",
         "images/post/tag_3-4_3.jpeg",
         "images/post/tag_3-4_4.jpeg",
         "images/post/tag_3-4_5.jpeg",
         "images/post/tag_3-4_6.jpeg",
         ]
author: "Sam"
date: 2022-03-31T01:00:00Z
categories: ["Italien"]
tags: ["Reise", "Fahrrad"]
featured: true
---

Kurz vor dem Einschlafen in unserem Zelt hinter Jesolo hörten wir noch ein Geräusch. Das war vermutlich nur ein Vogel, aber trotzdem gut genug für einen Cliffhanger ;)
Die erste Nacht im Zelt war ungewohnt, aber gut. Nach den ganzen Strapazen des Vortrages waren es sogar 12h. Daraufhin schwungen wir uns auf unsere Sättel und radelten mir Caro und Alex die italienischen Küsten-Ferienorte entlang. Jetzt, Ende März, sind diese menschenleer und es fällt schwer, sich vorzustellen, wie hier das Leben wuselt zur Hochsaison. Aufgrund einer gesperrten Brücke mussten wir einen Umweg von ca. 10 km nehmen. Daher entschieden wir uns später von der geplanten Route abzuweichen und besuchten Bibione daher nicht. Vermutlich wären dort die gleichen verlassenen Strandhotels zu sehen gewesen, wie in Jesolo. Unterwegs sahen wir noch eine Familie von erstaunlich fotogenen Bisamratten und beschlossen den Tag mit Kartenspielen mit Caro und Alex in unserem Zelt bei Precenicco.
Als wir am nächsten Morgen aufwachten, regnete es bereits in Strömen. Das macht nicht besonders Vorfreude auf den Tag, aber hilft ja nix! Es bleibt nichts anderes zu tun, als dich regendicht einzupacken und dem Wetter zu trotzen. Es hörte dann auch den ganzen Tag über nicht mehr auf zu regnen. Mittags gingen wir in ein Restaurant, um uns aufzuwärmen. Sonst hielten wir jedoch kaum an, denn jeder sehnte sich nach einer warmen Tasse Tee im Zelt. Am Ende schafften wir sogar 75 km und übernachten zwischen einer Autobahn und einer Bahnlinie hinter Monfalcone. Es ist zu erwarten, dass diese Nacht also nicht allzu erholsam wird…
