---
title: "Tag 67 und 68 | 03.-04.06. | insg. 30 km"
image: "images/post/tag67-und-68/tag67.jpeg"
images: ["images/post/tag67-und-68/tag67-und-68_0.jpeg",
         "images/post/tag67-und-68/tag67-und-68_1.jpeg",
         "images/post/tag67-und-68/tag67-und-68_2.jpeg",
         "images/post/tag67-und-68/tag67-und-68_3.jpeg",
         "images/post/tag67-und-68/tag67-und-68_4.jpeg",
         "images/post/tag67-und-68/tag67-und-68_5.jpeg",
         "images/post/tag67-und-68/tag67-und-68_6.jpeg",
         "images/post/tag67-und-68/tag67-und-68_7.jpeg",
         "images/post/tag67-und-68/tag67-und-68_8.jpeg",
         "images/post/tag67-und-68/tag67-und-68_9.jpeg",
         "images/post/tag67-und-68/tag67-und-68_10.jpeg",
         "images/post/tag67-und-68/tag67-und-68_11.jpeg"
        ]
author: "Sam"
date: 2022-06-04T23:00:00Z
categories: ["Türkei"]
tags: ["Reise", "Fahrrad", "Gaziantep", "Hopa"]
featured: true
---

Ganz benommen öffne ich gegen 8 Uhr meine Augen. Ich fühle mich nicht so gut. Die Fahrt gestern nach Gaziantep hat uns ganz schön geschafft. Es war heiß und sehr windig. Das hat zu einem "Staub-Sturm" geführt, durch den wir uns durchkämpfen mussten. Wir nehmen uns daher lange Zeit am Vormittag und machen eine Katzenwäsche am Spülbecken des Picknick-Platzes.

Aber wir sind ja in Gaziantep, um das weltberühmte Essen zu probieren. Daher brechen wir mittags auf in ein Restaurant. Dort treffen wir gleich eine deutsch-türkische Familie aus Köln. Besser hätte es nicht laufen können. Diese kommunizieren unsere Wünsche an den Kellner und beraten uns vorzüglich. Wir essen köstliche Linsensuppe, Kebap und Baklava. Am Ende lädt uns die Familie sogar ein! Wahnsinn!

Danach geht es in die Stadt. Wir probieren noch mehr Kebap aus, verbringen viel Zeit in einem Stadtpark und versuchen einen Teppich auf dem Bazar zu ersteigern. Am Ende war Raffi bei der Preisverhandlung jedoch etwas zu fordernd, sodass der Verkäufer den Teppich vor Wut durch den ganzen Laden schmeißt. Das war wohl nix. Nachts können wir dann in der Garage von Baris übernachten. Baris haben wir in Istanbul getroffen. Er hat uns damals zu sich eingeladen. Leider ist er zurzeit jedoch nicht Zuhause. Aber er war so großzügig, uns seine Garage anzubieten.

Am nächsten Tag schlendern wir nochmal über den Kupfer-Bazar. Für mich gibt es neue Schuhe, nachdem meine Sandalen sich langsam auflösen. Wir kaufen auch beide einen Teppich. Wir haben bei einem anderen Verkäufer sogar einen günstigeren Preis bekommen. Also, alles richtig gemacht. 

Abends steigen wir dann in einen Bus in den Norden der Türkei, nach Hopa. Die letzten Tage war es hier im Süden so heiß, dass uns das Radfahren keinen Spaß mehr gemacht hat. Würden wir von Gaziantep aus Richtung Osten weiterfahren, würde es im Iran sogar noch heißer werden. Wir haben daher unsere Route geändert und fahren über Georgien und Armenien in den Iran. Wir hören uns dann in Georgien…
